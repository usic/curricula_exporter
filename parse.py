#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import csv
import sys
import codecs
import MySQLdb

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')

def parse(fileobject):
    result = []
    for line in unicode_csv_reader(fileobject):
        result.append(line)
    return result

def write_html(filename, header, rows):
    out_file = codecs.open(filename, 'w', 'utf8')
    out_file.write('<!DOCTYPE html><html><head><meta charset="utf-8"></head>')
    out_file.write('<body><table border="1">')
    out_file.write('<tr>')
    for field in header:
        out_file.write('<th>%s</th>' % field)
    out_file.write('</tr>\n')
    for row in rows:
        out_file.write('<tr>')
        for field in row:
            out_file.write('<td>%s</td>' % field.replace('\n', '<br>'))
        out_file.write('</tr>\n')
    out_file.write('</table></body>')
    out_file.close()

def export_db(fields,rows):
    fields=",".join(fields)
    db = MySQLdb.connect("host","user","password","database", charset="utf8")
    cursor = db.cursor()
    cursor.execute("DROP TABLE IF EXISTS courses")
    sql = """CREATE TABLE courses (
              level varchar(255),
              code varchar(20),
              program varchar(255),
              course_code varchar(20) ,
              course varchar(255),
              type varchar(255),
              credits float,
              year float,
              term varchar(255),
              description text,
              faculty_short text,
              faculty_full text,
              faculty_en varchar(30)
              )"""
    cursor.execute(sql)
    for row in rows:
        if len(row)==11:
            row.pop()
        row=row+code2fac(row[1].strip())
        cursor.execute('INSERT INTO courses('+fields+')'  'VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',row)
    db.commit()
    cursor.close()

def code2fac(code):
    inp=dict([('fns',['6.040203','6.040106','6.040102','6.040101','8.04020301','8.04010601','8.04010213','8.04010201','8.04010101']), ('fi',['6.050103','6.040301','6.040302','8.04030101','8.04030302','8.05010101','8.05010301','7.05010101']),('fsnst',['8.13010201','8.03060101','6.130102','6.030104','6.030101','8.03010101','8.03010401','8.03030101']), ('fls',['6.030401','8.03040101','7.03040101']), ('fgn',['6.020101','6.020301','6.020302','6.020303','8.02010101','8.02030101','8.02030201','8.02030307','8.02030204']),('fes',['6.030508','6.030507','6.030501','8.03050801','8.03050101'])])
    for fac in inp:
        if code in inp[fac]:
            return fac2human(fac)

def fac2human(fac):
    data=dict([('fns',['ФПрН','Факультет природничих наук','FNS']),('fi',['ФІ','Факультет інтформатики','FI']),('fgn',['ФГН','Факультет гуманітарних наук','FGN']),('fls',['ФПвН','Факультет правничих наук','FLS']),('fes',['ФЕН','Факультет економічних наук','FES']),('fsnst',['ФСНСТ','Факультет соціальних наук і соціальних технологій','FSNST'])])
    return data[fac]

def main():
    input_filename = sys.argv[1]
    output_filename = sys.argv[2]
    rows = parse(codecs.open(input_filename, 'r', 'utf8'))
    courses_fields = ['level','code','program','course_code','course','type','credits','year','term','description','faculty_short','faculty_full','faculty_en']
    export_db(courses_fields,rows)
#    write_html(output_filename, courses_fields, rows)


if __name__ == '__main__':
    main()
